# README #

Skoobalon Classics is a silly side project to make ActionScript 3 reimplementations of all the silly games I made when I was learning to use C around the turn of the century. Given that most of these games were created in a single day, the reimplementation effort shouldn't be too difficult...

### How do I get set up? ###

The easiest way to get started is...

1. Install [FlashDevelop](http://www.flashdevelop.org/) and the latest version of the Flex SDK. (The installer should do this for you.)

2. Install the [standalone Flash player](http://www.adobe.com/support/flashplayer/downloads.html).

3. Open Skoobalon Classics.asproj and build the project.

4. Press F5. The project will build and start up automatically.

### Who do I talk to? ###

For more details, contact steven at skoobalon.com.