package com.skoobalon.classics {
	import flash.display.DisplayObject;
	
	/**
	 * @author Steven Wallace
	 */
	public interface IGame {
		
		function get width():int;
		
		function get height():int;
		
		function get date():Date;
		
		function get name():String;
		
		function get gameInstance():DisplayObject;
		
		function restart():void;
		
	}
	
}