package com.skoobalon.classics {
	
	import com.skoobalon.classics.event.MenuEvent;
	import com.skoobalon.classics.menu.GameContainer;
	import com.skoobalon.classics.menu.MainMenu;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	
	/**
	 * @author Steven Wallace
	 */
	public class Main extends Sprite {
		
		private var _menu:MainMenu;
		private var _game:GameContainer;
		
		public function Main():void {
			if (stage) {
				init();
			} else {
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			_menu = new MainMenu();
			_menu.addEventListener(MenuEvent.PLAY_GAME, onPlayGame);
			addChild(_menu);
			
			addEventListener(MenuEvent.RETURN_TO_MENU, onReturnToMenu, true);
		}
		
		private function onPlayGame(event:MenuEvent):void {
			trace("Playing", event.game.name);
			
			if (contains(_menu)) {
				removeChild(_menu);
			}
			
			if (_game && contains(_game)) {
				removeChild(_game);
			}
			
			_game = new GameContainer(event.game);
			addChild(_game);
			_game.x = int((stage.stageWidth - _game.width) / 2);
			_game.y = int((stage.stageHeight - _game.height) / 2);
		}
		
		private function onReturnToMenu(event:MenuEvent):void {
			if (_game && contains(_game)) {
				removeChild(_game);
				_game.dispose();
				_game = null;
			}
			
			stage.displayState = StageDisplayState.NORMAL;
			
			if (!contains(_menu)) {
				addChild(_menu);
			}
		}
		
	}
	
}