package com.skoobalon.classics.util {
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.net.URLRequest;
	
	/**
	 * @author Steven Wallace
	 */
	public class SkoobalonLoader extends Loader {
		
		public function SkoobalonLoader(url:String) {
			super();
			load(new URLRequest(url));
		}
		
		public function createNewBitmap():Bitmap {
			if (content is Bitmap) {
				return new Bitmap((content as Bitmap).bitmapData);
			} else {
				return null;
			}
		}
		
	}

}