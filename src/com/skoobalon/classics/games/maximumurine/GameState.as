package com.skoobalon.classics.games.maximumurine {
	
	/**
	 * @author Steven Wallace
	 */
	public class GameState {
		
		public static const INTRO_SLIDE_1:GameState = new GameState();
		public static const INTRO_SLIDE_2:GameState = new GameState();
		public static const PLAYGAME:GameState = new GameState();
		public static const END:GameState = new GameState();
		
		public function GameState() {
			
		}
		
	}

}