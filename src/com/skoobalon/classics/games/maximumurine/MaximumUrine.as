package com.skoobalon.classics.games.maximumurine {
	
	import com.skoobalon.classics.IGame;
	import flash.display.DisplayObject;
	
	/**
	 * Maximum Urine!
	 * @author Steven Wallace
	 */
	public class MaximumUrine implements IGame {
		
		private static const WIDTH:int = 640;
		private static const HEIGHT:int = 480;
		private static const DATE:Date = new Date(2000, 8, 10);
		private static const NAME:String = "Maximum Urine";
		
		private var _gameInstance:MaximumUrineGame;
		
		
		public function MaximumUrine() {
			// (Does nothing)
		}
		
		public function get width():int {
			return WIDTH;
		}
		
		public function get height():int {
			return HEIGHT;
		}
		
		public function get date():Date {
			return DATE;
		}
		
		public function get name():String {
			return NAME;
		}
		
		public function get gameInstance():DisplayObject {
			if (_gameInstance == null) {
				_gameInstance = new MaximumUrineGame();
			}
			
			return _gameInstance;
		}
		
		public function restart():void {
			MaximumUrineGame(gameInstance).restart();
		}
		
	}

}
