package com.skoobalon.classics.games.maximumurine {
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * @author Steven Wallace
	 */
	public class Player extends Sprite {
		
		private var _playerImage:DisplayObject;
		private var _crosshairImage:DisplayObject;
		private var _faceImage:DisplayObject;
		
		private var _piss:Shape = new Shape();
		private var _pissAnchor:Point;
		
		private var _targetRect:Rectangle;
		private var _goalHeight:int;
		
		
		public function Player() { }
		
		public function build():void {
			addChild(_faceImage);
			addChild(_piss);
			addChild(_playerImage);
			addChild(_crosshairImage);
		}
		
		public function isInTarget():Boolean {
			return _targetRect.contains(_crosshairImage.x + 7, _crosshairImage.y + 7);
		}
		
		public function isAtGoal():Boolean {
			return _faceImage.y + 7 <= _goalHeight;
		}
		
		public function left():void {
			_crosshairImage.x--;
			updatePiss();
		}
		
		public function right():void {
			_crosshairImage.x++;
			updatePiss();
		}
		
		public function up():void {
			_crosshairImage.y--;
			updatePiss();
		}
		
		public function down():void {
			_crosshairImage.y++;
			updatePiss();
		}
		
		public function moveTargetUp():void {
			_faceImage.y--;
		}
		
		private function updatePiss():void {
			_piss.graphics.clear();
			_piss.graphics.moveTo(_pissAnchor.x, _pissAnchor.y);
			_piss.graphics.lineStyle(1, 0xD6A600);
			_piss.graphics.lineTo(_crosshairImage.x + 7, _crosshairImage.y + 7);
		}
		
		public function setCharacter(image:DisplayObject, startX:int, startY:int):void {
			_playerImage = image;
			_playerImage.x = startX;
			_playerImage.y = startY;
		}
		
		public function ml():void { _playerImage.x--; tt(); }
		public function mr():void { _playerImage.x++; tt(); }
		public function mu():void { _playerImage.y--; tt(); }
		public function md():void { _playerImage.y++; tt(); }
		private function tt():void { trace("(" + _playerImage.x + "," + _playerImage.y + ")"); }
		
		public function setCrosshair(image:DisplayObject, startX:int, startY:int):void {
			_crosshairImage = image;
			_crosshairImage.x = startX - 7;
			_crosshairImage.y = startY - 7;
		}
		
		public function setFace(image:DisplayObject, startX:int, startY:int):void {
			_faceImage = image;
			_faceImage.x = startX;
			_faceImage.y = startY;
		}
		
		public function set pissAnchor(value:Point):void {
			_pissAnchor = value;
		}
		
		public function set targetRect(value:Rectangle):void {
			_targetRect = value;
		}
		
		public function set goalHeight(value:Number):void {
			_goalHeight = value;
		}
		
	}

}