package com.skoobalon.classics.games.maximumurine {
	
	import com.skoobalon.classics.util.SkoobalonLoader;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import mx.core.SpriteAsset;
	
	
	/**
	 * @author Steven Wallace
	 */
	public class MaximumUrineGame extends Sprite {
		
		private const ASSETROOT:String = "maximumurine/";
		
		private var _gameState:GameState = null;
		
		private var _introSlide1:SkoobalonLoader;
		private var _introSlide2:SkoobalonLoader;
		private var _bg:SkoobalonLoader;
		private var _player1Image:SkoobalonLoader;
		private var _player2Image:SkoobalonLoader;
		private var _player3Image:SkoobalonLoader;
		private var _player4Image:SkoobalonLoader;
		private var _crosshair:SkoobalonLoader;
		private var _face:SkoobalonLoader;
		
		private var _player1:Player;
		private var _player2:Player;
		private var _player3:Player;
		private var _player4:Player;
		
		private var _winner:int;
		private var _winText:TextField;
		
		public function MaximumUrineGame() {
			// TODO: Loading screen?
			trace("Loading assets");
			_introSlide1 = new SkoobalonLoader(ASSETROOT + "intro1.jpg");
			_introSlide2 = new SkoobalonLoader(ASSETROOT + "intro2.jpg");
			_bg = new SkoobalonLoader(ASSETROOT + "BG.jpg");
			_player1Image = new SkoobalonLoader(ASSETROOT + "PL1.png");
			_player2Image = new SkoobalonLoader(ASSETROOT + "PL2.png");
			_player3Image = new SkoobalonLoader(ASSETROOT + "PL3.png");
			_player4Image = new SkoobalonLoader(ASSETROOT + "PL4.png");
			_crosshair = new SkoobalonLoader(ASSETROOT + "CH.png");
			_face = new SkoobalonLoader(ASSETROOT + "smile.png");
		}
		
		public function restart():void {
			if (_gameState == GameState.INTRO_SLIDE_1) {
				// Do nothing if we're already at the beginning
				return;
			}
			
			// Make sure to remove all children
			while (numChildren > 0) {
				removeChildAt(0);
			}
			
			_winText = new TextField();
			const tf:TextFormat = new TextFormat("Lucida Handwriting", 28, 0x00455A, true, true);
			tf.align = TextFormatAlign.CENTER;
			_winText.defaultTextFormat = tf;
			_winText.multiline = true;
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			
			changeGameState(GameState.INTRO_SLIDE_1);
		}
		
		private function onAddedToStage(event:Event):void {
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onRemovedFromStage(event:Event):void {
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		public function dispose():void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		private function onEnterFrame(event:Event):void {
			switch (_gameState) {
				case GameState.INTRO_SLIDE_1:
				case GameState.INTRO_SLIDE_2:
					// (No logic)
					break;
					
				case GameState.PLAYGAME:
					checkTargets();
					break;
					
				defalt:
					trace("Unhandled enter frame");
			}
		}
		
		private function checkTargets():void {
			// Update targets as applicable
			if (_player1.isInTarget()) {
				_player1.moveTargetUp();
			}
			
			if (_player2.isInTarget()) {
				_player2.moveTargetUp();
			}
			
			if (_player3.isInTarget()) {
				_player3.moveTargetUp();
			}
			
			if (_player4.isInTarget()) {
				_player4.moveTargetUp();
			}
			
			// Check for game over
			if (_player1.isAtGoal()) {
				_winner = 1;
				changeGameState(GameState.END);
			} else if (_player2.isAtGoal()) {
				_winner = 2;
				changeGameState(GameState.END);
			} else if (_player3.isAtGoal()) {
				_winner = 3;
				changeGameState(GameState.END);
			} else if (_player4.isAtGoal()) {
				_winner = 4;
				changeGameState(GameState.END);
			}
		}
		
		private var _keys:Array /* of Boolean */ = [];
		private function onKeyDown(event:KeyboardEvent):void {
			switch (_gameState) {
				case GameState.INTRO_SLIDE_1:
					if (!_keys[event.keyCode]) {
						changeGameState(GameState.INTRO_SLIDE_2);
					}
					break;
					
				case GameState.INTRO_SLIDE_2:
					if (!_keys[event.keyCode]) {
						changeGameState(GameState.PLAYGAME);
					}
					break;
					
				case GameState.PLAYGAME:
					switch (event.keyCode) {
						case Keyboard.A:	_player1.left();	break;
						case Keyboard.D:	_player1.right();	break;
						case Keyboard.W:	_player1.up();		break;
						case Keyboard.S:	_player1.down();	break;
							
						case Keyboard.F:	_player2.left();	break;
						case Keyboard.H:	_player2.right();	break;
						case Keyboard.T:	_player2.up();		break;
						case Keyboard.G:	_player2.down();	break;
							
						case Keyboard.J:	_player3.left();	break;
						case Keyboard.L:	_player3.right();	break;
						case Keyboard.I:	_player3.up();		break;
						case Keyboard.K:	_player3.down();	break;
							
						case Keyboard.LEFT:	_player4.left();	break;
						case Keyboard.RIGHT:_player4.right();	break;
						case Keyboard.UP:	_player4.up();		break;
						case Keyboard.DOWN:	_player4.down();	break;
					}
					break;
					
				case GameState.END:
					if (!_keys[event.keyCode]) {
						changeGameState(GameState.PLAYGAME);
					}
					break;
					
				default:
					trace("key down not handled!");
			}
			
			_keys[event.keyCode] = true;
		}
		
		private function onKeyUp(event:KeyboardEvent):void {
			_keys[event.keyCode] = false;
		}
		
		private function changeGameState(newState:GameState):void {
			if (newState != _gameState) {
				// Dispose of current state
				switch (_gameState) {
					case GameState.INTRO_SLIDE_1:
						removeChild(_introSlide1);
						break;
						
					case GameState.INTRO_SLIDE_2:
						removeChild(_introSlide2);
						break;
						
					case GameState.PLAYGAME:
						// We clear everything after the endgame phase
						break;
						
					case GameState.END:
						removeChild(_winText);
				}
				
				// Load new state
				_gameState = newState;
				switch (_gameState) {
					case GameState.INTRO_SLIDE_1:
						addChild(_introSlide1);
						break;
						
					case GameState.INTRO_SLIDE_2:
						addChild(_introSlide2);
						break;
						
					case GameState.PLAYGAME:
						// Initialize players
						initializePlayers();
						
						// Add graphics
						addChild(_bg);
						addChild(_player1);
						addChild(_player2);
						addChild(_player3);
						addChild(_player4);
						break;
						
					case GameState.END:
						_winText.width = _bg.width;
						_winText.text = "Player " + _winner + " won!\nPress ENTER to play again.";
						_winText.y = int((_bg.height - _winText.textHeight) / 2);
						addChild(_winText);
						break;
				}
			}
		}
		
		private function initializePlayers():void {
			// Player 1
			_player1 = new Player();
			
			_player1.setCharacter(_player1Image.createNewBitmap(), 27, 307);
			_player1.setCrosshair(_crosshair.createNewBitmap(), 94, 376);
			_player1.setFace(_face.createNewBitmap(), 77, 233);
			_player1.pissAnchor = new Point(57, 440);
			_player1.targetRect = new Rectangle(94, 274, 4, 4);
			_player1.goalHeight = 115;
			_player1.build();
			
			// Player 2
			_player2 = new Player();
			_player2.setCharacter(_player2Image.createNewBitmap(), 186, 301);
			_player2.pissAnchor = new Point(211, 430);
			_player2.setCrosshair(_crosshair.createNewBitmap(), 223, 373);
			_player2.targetRect = new Rectangle(221, 271, 4, 4);
			_player2.goalHeight = 115;
			_player2.setFace(_face.createNewBitmap(), 206, 233);
			_player2.build();
			
			// Player 3
			_player3 = new Player();
			_player3.setCharacter(_player3Image.createNewBitmap(), 305, 314);
			_player3.pissAnchor = new Point(341, 434);
			_player3.setCrosshair(_crosshair.createNewBitmap(), 348, 371);
			_player3.targetRect = new Rectangle(346, 269, 4, 4);
			_player3.goalHeight = 115;
			_player3.setFace(_face.createNewBitmap(), 331, 233);
			_player3.build();
			
			// Player 4
			_player4 = new Player();
			_player4.setCharacter(_player4Image.createNewBitmap(), 415, 301);
			_player4.pissAnchor = new Point(470, 444);
			_player4.setCrosshair(_crosshair.createNewBitmap(), 476, 370);
			_player4.targetRect = new Rectangle(474, 268, 4, 4);
			_player4.goalHeight = 115;
			_player4.setFace(_face.createNewBitmap(), 457, 233);
			_player4.build();
		}
		
	}

}