package com.skoobalon.classics.event {
	import com.skoobalon.classics.IGame;
	import flash.events.Event;
	
	/**
	 * @author Steven Wallace
	 */
	public class MenuEvent extends Event {
		
		public static const PLAY_GAME:String = "playGame";
		public static const RETURN_TO_MENU:String = "returnToMenu";
		public static const TOGGLE_FULLSCREEN:String = "toggleFullscreen";
		
		private var _game:IGame;
		
		public function MenuEvent(type:String, game:IGame, bubbles:Boolean=false, cancelable:Boolean=false) { 
			super(type, bubbles, cancelable);
			_game = game;
		}
		
		public override function clone():Event { 
			return new MenuEvent(type, _game, bubbles, cancelable);
		}
		
		public function get game():IGame {
			return _game;
		}
		
	}
	
}