package com.skoobalon.classics.menu {
	
	import com.skoobalon.classics.event.MenuEvent;
	import com.skoobalon.classics.IGame;
	import com.skoobalon.classics.games.maximumurine.MaximumUrine;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * @author Steven Wallace
	 */
	public class MainMenu extends Sprite {
		
		private static const ALL_GAMES:Vector.<IGame> = Vector.<IGame>([
			new MaximumUrine()
		]);
		
		private var _header:TextField;
		
		
		public function MainMenu() {
			_header = new TextField();
			const tf:TextFormat = new TextFormat("Verdana", 30, 0, true, null, null, null, null, TextFormatAlign.CENTER);
			_header.defaultTextFormat = tf;
			_header.mouseEnabled = false;
			_header.autoSize = TextFieldAutoSize.CENTER;
			_header.text = "Skoobalon Classics";
			addChild(_header);
			
			for (var i:int = 0; i < ALL_GAMES.length; i++) {
				const game:IGame = ALL_GAMES[i];
				const mi:MenuItem = new MenuItem(game);
				
				mi.x = 25 + mi.width * Math.floor(i / 2);
				mi.y = 50 + mi.height * Math.floor(i % 2);
				mi.addEventListener(MouseEvent.CLICK, onMenuItemClicked);
				addChild(mi);
			}
		}
		
		private function onMenuItemClicked(event:MouseEvent):void {
			const mi:MenuItem = event.target as MenuItem;
			dispatchEvent(new MenuEvent(MenuEvent.PLAY_GAME, mi.game));
		}
		
	}

}