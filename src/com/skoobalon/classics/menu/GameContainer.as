package com.skoobalon.classics.menu {
	import com.skoobalon.classics.event.MenuEvent;
	import com.skoobalon.classics.IGame;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * @author Steven Wallace
	 */
	public class GameContainer extends Sprite {
		
		private var _game:IGame;
		private var _frame:Shape;
		
		private var _back:TextButton;
		private var _fullscreen:TextButton;
		
		public function GameContainer(game:IGame) {
			_game = game;
			_game.restart();
			
			_frame = new Shape();
			_frame.graphics.lineStyle(3);
			_frame.graphics.drawRect(0, 0, _game.width + 3, _game.height + 3);
			
			_game.gameInstance.x = 2;
			_game.gameInstance.y = 2;
			
			addChild(_frame);
			addChild(_game.gameInstance);
			
			_back = new TextButton();
			_back.text = "Return To Menu";
			_back.addEventListener(MouseEvent.CLICK, onBackClicked);
			_back.x = _game.gameInstance.x + int(_game.width / 2) - 8 - _back.width;
			_back.y = game.height + 16;
			addChild(_back);
			
			_fullscreen = new TextButton();
			_fullscreen.text = "Toggle Fullscreen";
			_fullscreen.addEventListener(MouseEvent.CLICK, onFullscreenClicked);
			_fullscreen.x = _game.gameInstance.x + int(_game.width / 2) + 8;
			_fullscreen.y = _back.y;
			addChild(_fullscreen);
		}
		
		public function dispose():void {
			_back.removeEventListener(MouseEvent.CLICK, onBackClicked);
			removeChild(_back);
			_back = null;
			
			_fullscreen.removeEventListener(MouseEvent.CLICK, onFullscreenClicked);
			removeChild(_fullscreen);
			_fullscreen = null;
			
			removeChild(_game.gameInstance);
			removeChild(_frame);
			_game = null;
			_frame = null;
		}
		
		private function onBackClicked(event:MouseEvent):void {
			dispatchEvent(new MenuEvent(MenuEvent.RETURN_TO_MENU, _game));
		}
		
		private function onFullscreenClicked(event:MouseEvent):void {
			if (stage.displayState == StageDisplayState.NORMAL) {
				const pt:Point = _game.gameInstance.localToGlobal(new Point(0, 0));
				stage.fullScreenSourceRect = new Rectangle(pt.x, pt.y, _game.width, _game.height);
				stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			} else {
				stage.displayState = StageDisplayState.NORMAL;
			}
		}
		
	}

}