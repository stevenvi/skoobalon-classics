package com.skoobalon.classics.menu {
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * @author Steven Wallace
	 */
	public class TextButton extends Sprite {
		
		private static const STATE_IDLE:int = 0;
		private static const STATE_OVER:int = 1;
		private static const STATE_DOWN:int = 2;
		private var _state:int;
		
		private var _idle:Shape;
		private var _over:Shape;
		private var _down:Shape;
		
		private var _text:TextField;
		
		public function TextButton() {
			_idle = makeButton(0xCCCCCC);
			_over = makeButton(0x999999);
			_down = makeButton(0x777777);
			
			addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			addChild(_idle);
			_state = STATE_IDLE;
			
			_text = new TextField();
			const tf:TextFormat = new TextFormat(null, 14);
			_text.defaultTextFormat = tf;
			_text.width = _idle.width;
			_text.height = _idle.height;
			_text.mouseEnabled = false;
			addChild(_text);
		}
		
		public function set text(value:String):void {
			_text.text = value;
			_text.x = int((_text.width - _text.textWidth) / 2);
			_text.y = int((_text.height - _text.textHeight) / 2);
		}
		
		private function makeButton(color:uint):Shape {
			const shape:Shape = new Shape();
			shape.graphics.lineStyle(1);
			shape.graphics.beginFill(color);
			shape.graphics.drawRect(0, 0, 200, 30);
			return shape;
		}
		
		public function dispose():void {
			removeEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			removeEventListener(MouseEvent.MOUSE_OUT, mouseOut);
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		private function changeState(newState:int):void {
			if (_state != newState) {
				removeChildAt(0);
				switch (newState) {
					case STATE_IDLE:	addChildAt(_idle, 0);	break;
					case STATE_OVER:	addChildAt(_over, 0);	break;
					case STATE_DOWN:	addChildAt(_down, 0);	break;
				}
				_state = newState;
			}
		}
		
		private function mouseOver(event:MouseEvent):void {
			changeState(STATE_OVER);
		}
		
		private function mouseOut(event:MouseEvent):void {
			changeState(STATE_IDLE);
		}
		
		private function mouseDown(event:MouseEvent):void {
			changeState(STATE_DOWN);
		}
		
		private function mouseUp(event:MouseEvent):void {
			changeState(STATE_OVER);
		}
		
		override public function get width():Number {
			return _idle.width;
		}
		
		override public function get height():Number {
			return _idle.height;
		}
		
	}

}