package com.skoobalon.classics.menu {
	import com.skoobalon.classics.IGame;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import mx.formatters.DateFormatter;
	
	/**
	 * @author Steven Wallace
	 */
	public class MenuItem extends Sprite {
		
		private var _game:IGame;
		private var _bg:Shape;
		private var _title:TextField;
		
		public function MenuItem(game:IGame) {
			_game = game;
			
			_bg = new Shape();
			_bg.graphics.lineStyle(2);
			_bg.graphics.drawRect(0, 0, 320, 240);
			_bg.graphics.beginFill(0, 0.2);
			_bg.graphics.drawRect(4, 4, 312, 232);
			addChild(_bg);
			
			_title = new TextField();
			const tf:TextFormat = new TextFormat("Arial", 14, 0, null, null, null, null, null, TextFormatAlign.CENTER);
			_title.defaultTextFormat = tf;
			_title.mouseEnabled = false;
			_title.width = 300;
			_title.height = 220;
			const df:DateFormatter = new DateFormatter();
			df.formatString = "EEEE, MMMM D, YYYY";
			_title.text = game.name + "\n" + df.format(game.date);
			_title.x = 10;
			_title.y = (_title.height - _title.textHeight) / 2;
			addChild(_title);
		}
		
		public function get game():IGame {
			return _game;
		}
		
	}

}